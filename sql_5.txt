SELECT actors.full_name
FROM cast
INNER JOIN actors ON cast.actor_id=actors.id 
INNER JOIN movies ON cast.movie_id=movies.id
WHERE movies.title IN ('Star Wars','Return of the Jedi') 
GROUP BY actors.full_name
HAVING COUNT(movie_id) = 2 ;
# this query return empty set so no need to go further, nobody play in these two movies


SELECT actors.full_name, COUNT(distinct(genre)) as numb_dist_genre
FROM cast
INNER JOIN actors ON cast.actor_id=actors.id 
INNER JOIN movies ON cast.movie_id=movies.id
GROUP BY actors.full_name 
ORDER BY numb_dist_genre desc 
limit 5 ;
# Kevin Bacon 6 ;Bill Paxton  3 ;Stevo Polyi 3;Michael Madsen 3 ; Brad Pitt  3


SELECT full_name,year,film_by_year
FROM ( 
	SELECT full_name,year,count(distinct(movie_id)) as film_by_year
	FROM cast
	INNER JOIN actors ON cast.actor_id=actors.id 
	INNER JOIN movies ON cast.movie_id=movies.id
	GROUP BY Year,full_name
	) as table_film
WHERE film_by_year = (SELECT max(film_by_year)
					  FROM ( SELECT full_name,year,count(distinct(movie_id)) as film_by_year
							 FROM cast
							 INNER JOIN actors ON cast.actor_id=actors.id 
							 INNER JOIN movies ON cast.movie_id=movies.id
							 GROUP BY Year,full_name 
							) as table_max_film )  ;
#Cameron Diaz and 2 movies


SELECT AVG(career)
FROM (
SELECT full_name, count(distinct(movie_id)) as total_film , max(year) - min(year) as career
FROM cast
INNER JOIN actors ON cast.actor_id=actors.id 
INNER JOIN movies ON cast.movie_id=movies.id
GROUP BY full_name
HAVING total_film >= 5 ) as table_career ;
#25 years, Nobody as exactly 5 movie only Kevin Bacon has more than 5 movie


SELECT full_name, hiatus 
FROM (
SELECT t1.full_name , t1.year AS new_date , t2.year AS previous_date , t1.year - t2.year AS hiatus 
FROM (SELECT full_name, title, year
FROM cast
INNER JOIN actors ON cast.actor_id=actors.id 
INNER JOIN movies ON cast.movie_id=movies.id )AS t1
CROSS JOIN (SELECT full_name, title, year
FROM cast
INNER JOIN actors ON cast.actor_id=actors.id 
INNER JOIN movies ON cast.movie_id=movies.id ) AS t2 
ON t1.full_name = t2.full_name
AND t2.year = (SELECT MAX(t3.year) FROM (SELECT full_name, title, year
FROM cast
INNER JOIN actors ON cast.actor_id=actors.id 
INNER JOIN movies ON cast.movie_id=movies.id ) AS t3 WHERE t3.full_name=t1.full_name and t3.year<t1.year)) AS hiatus_table 
order by hiatus desc
limit 1 ;
#Malcolm Tierney |     18 years